==================================================================
https://keybase.io/iccub
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of http://michalbuczek.com
  * I am iccub (https://keybase.io/iccub) on keybase.
  * I have a public key ASDdpmb6tpyP6ue4UK0Idx1dWE28HCF4qYdfILwXXTqCBgo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0120dda666fab69c8feae7b850ad08771d5d584dbc1c2178a9875f20bc175d3a82060a",
      "host": "keybase.io",
      "kid": "0120dda666fab69c8feae7b850ad08771d5d584dbc1c2178a9875f20bc175d3a82060a",
      "uid": "977c121264dc2726a00597e8db295519",
      "username": "iccub"
    },
    "merkle_root": {
      "ctime": 1507304689,
      "hash": "3280020a73a367a2376f402f1794946fa01b67212a809f3c94010d4bb4be84c519385574d83a90678755582329394db7e9cff758f2f566bfd7624505e589971d",
      "hash_meta": "2ce4ae6a1464f449c261e9a91795e6b68fbf5cca52e28285373522a3f883ff9d",
      "seqno": 1527618
    },
    "service": {
      "hostname": "michalbuczek.com",
      "protocol": "http:"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "client": {
    "name": "keybase.io go client",
    "version": "1.0.33"
  },
  "ctime": 1507304727,
  "expire_in": 504576000,
  "prev": "87ba153113b99acd17edb31c27eef242e2f4d6e80235e3b5221b40f312359723",
  "seqno": 4,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEg3aZm+racj+rnuFCtCHcdXVhNvBwheKmHXyC8F106ggYKp3BheWxvYWTFA0R7ImJvZHkiOnsia2V5Ijp7ImVsZGVzdF9raWQiOiIwMTIwZGRhNjY2ZmFiNjljOGZlYWU3Yjg1MGFkMDg3NzFkNWQ1ODRkYmMxYzIxNzhhOTg3NWYyMGJjMTc1ZDNhODIwNjBhIiwiaG9zdCI6ImtleWJhc2UuaW8iLCJraWQiOiIwMTIwZGRhNjY2ZmFiNjljOGZlYWU3Yjg1MGFkMDg3NzFkNWQ1ODRkYmMxYzIxNzhhOTg3NWYyMGJjMTc1ZDNhODIwNjBhIiwidWlkIjoiOTc3YzEyMTI2NGRjMjcyNmEwMDU5N2U4ZGIyOTU1MTkiLCJ1c2VybmFtZSI6ImljY3ViIn0sIm1lcmtsZV9yb290Ijp7ImN0aW1lIjoxNTA3MzA0Njg5LCJoYXNoIjoiMzI4MDAyMGE3M2EzNjdhMjM3NmY0MDJmMTc5NDk0NmZhMDFiNjcyMTJhODA5ZjNjOTQwMTBkNGJiNGJlODRjNTE5Mzg1NTc0ZDgzYTkwNjc4NzU1NTgyMzI5Mzk0ZGI3ZTljZmY3NThmMmY1NjZiZmQ3NjI0NTA1ZTU4OTk3MWQiLCJoYXNoX21ldGEiOiIyY2U0YWU2YTE0NjRmNDQ5YzI2MWU5YTkxNzk1ZTZiNjhmYmY1Y2NhNTJlMjgyODUzNzM1MjJhM2Y4ODNmZjlkIiwic2Vxbm8iOjE1Mjc2MTh9LCJzZXJ2aWNlIjp7Imhvc3RuYW1lIjoibWljaGFsYnVjemVrLmNvbSIsInByb3RvY29sIjoiaHR0cDoifSwidHlwZSI6IndlYl9zZXJ2aWNlX2JpbmRpbmciLCJ2ZXJzaW9uIjoxfSwiY2xpZW50Ijp7Im5hbWUiOiJrZXliYXNlLmlvIGdvIGNsaWVudCIsInZlcnNpb24iOiIxLjAuMzMifSwiY3RpbWUiOjE1MDczMDQ3MjcsImV4cGlyZV9pbiI6NTA0NTc2MDAwLCJwcmV2IjoiODdiYTE1MzExM2I5OWFjZDE3ZWRiMzFjMjdlZWYyNDJlMmY0ZDZlODAyMzVlM2I1MjIxYjQwZjMxMjM1OTcyMyIsInNlcW5vIjo0LCJ0YWciOiJzaWduYXR1cmUifaNzaWfEQKB/rKmJKMEMxGbUBp3Lm4gswSBxnehT4Tp3wmzaujbtZjK2wG2KJSXaAKkXPlG/2yr0ZzOVmviF2CPW73kOmA+oc2lnX3R5cGUgpGhhc2iCpHR5cGUIpXZhbHVlxCD5Hl4cTW799wuljrjWgOBHTjUKfOSheyOQHc11u8Bkg6N0YWfNAgKndmVyc2lvbgE=

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/iccub

==================================================================